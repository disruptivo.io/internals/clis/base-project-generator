const fs = require('fs')
/**
 * It exports all that is located where the script is
 */
const exportModulesInTheCurrentLocation = () => {
  fs.readdirSync(`${__dirname}`).forEach((file) => {
    const moduleName = file.replace('.js', '')
    if (file.match(/\.js$/) !== null && file !== 'index.js') {
      exports[moduleName] = require(`./${moduleName}`)
    }
  })
}

exportModulesInTheCurrentLocation()
