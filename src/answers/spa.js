const Inquirer = require('../utils/inquirer')
const choices = require('./spaChoices')
const logger = require('../utils/logger')

const doQuestions = async () => {
  return await new Inquirer()
  .list({
    name: 'typeOfStarter',
    message: logger.accent1('what type of starter/template do you want to generate?'),
    choices: Object.keys(choices).map((key) => choices[key]),
  })
  .getAnswers()
}

module.exports = async () => {
  try {
    const { typeOfStarter } = await doQuestions()
    await require(`./spaAnswers/${typeOfStarter}.js`)()
  } catch (error) {
    logger.primary(error.message)
  }
}