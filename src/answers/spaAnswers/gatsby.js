const Inquirer = require('../../utils/inquirer')
const logger = require('../../utils/logger')
const { execSync } = require('child_process')
const { writeFileSync } = require('fs')

const doQuestions = async () => {
  return await new Inquirer()
  .input({
    name: 'username',
    message: logger.accent1('what is your username of Gitlab?'),
  })
  .input({
    name: 'projectName',
    message: logger.accent1('name of your project'),
  })
  .input({
    name: 'projectDescription',
    message: logger.accent1('description of your project'),
  })
  .input({
    name: 'projectRepository',
    message: logger.accent1('Repository of your project'),
  })
  .getAnswers()
}

const cloneProject = ({ username, projectName }) => {
  execSync(`cd ${process.cwd()} && git clone https://${username}@gitlab.com/gatsby-templates/base.git ${projectName}`)
}

const cleanProject = ({ projectName }) => {
  execSync(`cd ${process.cwd()}/${projectName} && rm -rf .git CHANGELOG.md README.md`)
}

const updatePackageFile = ({ username, projectName, projectDescription, projectRepository }) => {
  const fileName = `${process.cwd()}/${projectName}/package.json`
  const packageJson = require(fileName)

  packageJson.version = '0.0.0'
  packageJson.author = username
  packageJson.name = projectName
  packageJson.description = projectDescription
  packageJson.repository.url = projectRepository
  packageJson.bugs.url = `${projectRepository}/issues`
  packageJson.homepage = `${projectRepository}#readme`
  
  writeFileSync(fileName, JSON.stringify(packageJson, null, 1))
}

const updateReadmeFile = ({ projectName }) => {
  execSync(`cd ${process.cwd()}/${projectName} && echo 'this template was bootstrapped with [this](https://gitlab.com/gatsby-templates/base) starter' > Readme.md`)
}

const initializeGit = ({ projectName }) => {
  execSync(`cd ${process.cwd()}/${projectName} && git init && git add . && git commit -m "project initialized"`)
}

module.exports = async () => {
  try {
    const answers = await doQuestions()
    cloneProject(answers)
    cleanProject(answers)
    updatePackageFile(answers)
    updateReadmeFile(answers)
    initializeGit(answers)
  } catch (error) {
    logger.primary(error.message)
  }
}