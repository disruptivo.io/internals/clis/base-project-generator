module.exports = {
  primary: '#CE8CA3',
  primaryLight: '#D8B9C3',
  accent1: '#CD5680',
  accent2: '#2F455C',
  dark1: '#343434',
  dark2: '#959595',
}