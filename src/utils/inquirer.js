const inquirer = require('inquirer')

class Inquirer {

  constructor() {
    this.questions = []
    this.prompt = inquirer.createPromptModule();
  }

  addAnswer = (args) => {
    this.questions.push({
      ...args,
    })
  }

  input = (args) => {
    this.addAnswer({...args, type: 'input'})

    return this
  }
  
  list = (args) => {
    this.addAnswer({...args, type: 'list'})

    return this
  }

  getAnswers = async () =>{
    return await this.prompt(this.questions)
  }
}

module.exports = Inquirer