const log = console.log
const chalk = require('chalk')
const boxen = require('boxen')
const { primary, accent1, primaryLight  } = require('../constants/colors')

module.exports = {
  primary: (...message) => {
    log(chalk.hex(primary)(...message))
  },
  primaryLight: (...message) => {
    log(chalk.hex(primaryLight)(...message))
  },
  accent1: (...message) => {
    log(chalk.hex(accent1)(...message))
  },
  box: (...message) => {
    log(chalk.hex(primaryLight)(boxen(...message, { float: 'center', padding: { top: 1, bottom: 1, left: 10, right: 10 }, borderStyle: 'classic' })))
  },
}