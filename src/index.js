#!/usr/bin/env node
const Inquirer = require('./utils/inquirer')
const choices = require('./choices')
const logger = require('./utils/logger')

const doQuestions = async () => {
  return await new Inquirer()
  .list({
    name: 'typeOfProject',
    message: logger.accent1('what type of project do you want to generate?'),
    choices: Object.keys(choices).map((key) => choices[key]),
  })
  .getAnswers()
}

(async () => {
  try {
    logger.box('Base Project Generator')
    const { typeOfProject } = await doQuestions()
    await require(`./answers/${typeOfProject}.js`)()
  } catch (error) {
    logger.primary(error.message)
  }
})()