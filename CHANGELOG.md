#### 1.1.1 (2021-09-16)

##### Bug Fixes

*  remove link-module-alias package ([1242e33d](https://gitlab.com/luisbar/base-project-generator/commit/1242e33d2b45023005dc30f1d229f9f6848d2ff5))

### 1.1.0 (2021-09-16)

##### New Features

*  remove changelog file and add remote when generating a plain react project ([2593b28e](https://gitlab.com/luisbar/base-project-generator/commit/2593b28e6fe2f365f82148cd8fad6a3360006727))

##### Bug Fixes

*  move link module alias to dev dependencies in order to avoid error while linking the tool ([78eb4311](https://gitlab.com/luisbar/base-project-generator/commit/78eb431174df9b0d0d16af09a8a3a2b8e439cffd))

#### 1.0.2 (2020-10-13)

##### Documentation Changes

*  readme has been updated in order to inform how to publish a new version, and some caveats ([b9db679e](https://gitlab.com/luisbar/base-project-generator/commit/b9db679e3b1950d01bf10e8d27d4fa1520b4848c))

#### 1.0.1 (2020-10-13)

##### Chores

*  link-module-alias has been moved from devDependencies to dependencies ([c5141211](https://gitlab.com/luisbar/base-project-generator/commit/c514121105465fa6d37ed1e11801308e5b521d7b))
*  package.json file has been updated in order to publish the package to npm registry ([62f9d9c6](https://gitlab.com/luisbar/base-project-generator/commit/62f9d9c69a5d9583b155b560620add9c5bca0264))

## 1.0.0 (2020-10-12)

##### Documentation Changes

*  readme has been added ([b047c57f](https://gitlab.com/luisbar/base-project-generator/commit/b047c57f9069e6331ca8df791dab3e87122484b4))

##### New Features

*  generate-base-project command has been created ([2b6ed1ca](https://gitlab.com/luisbar/base-project-generator/commit/2b6ed1cae699e2b2eddded9fff106be3adef15c7))
*  script for generating a starter with just React has been done ([c8045812](https://gitlab.com/luisbar/base-project-generator/commit/c804581235ba9f0bffbb8041f681507b3b54dacb))
*  script for generating a Gatsby starter has been changed ([ce12eefb](https://gitlab.com/luisbar/base-project-generator/commit/ce12eefb1b22b2e4227921f195970df55545a1ab))
*  commitizen has been installed ([6fe5a4b8](https://gitlab.com/luisbar/base-project-generator/commit/6fe5a4b838ca1e706164b7d42978253453d76b49))
*  changelog generator has been added ([5922de6a](https://gitlab.com/luisbar/base-project-generator/commit/5922de6a9abf6d41739b94c6a6cde7c08632d075))
*  library for configuring alias has been added and configured ([e07ef30c](https://gitlab.com/luisbar/base-project-generator/commit/e07ef30c1d6993dd17db05f3582e65b4c9ef7335))
*  script for generating an SPA Gatsby starter has been done ([d5b9a9d1](https://gitlab.com/luisbar/base-project-generator/commit/d5b9a9d19ac22320f03c21e6b76216785eafa43f))

