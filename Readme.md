## How to run

### Development mode

1. **Install nvm**
    
    Instructions [here](https://github.com/nvm-sh/nvm#installing-and-updating)

2. **Clone the project**
    ```bash
    git clone https://gitlab.com/luisbar/base-project-generator.git && cd base-project-generator
    ```

3. **Install dependencies**
    ```bash
    nvm use && npm i && npm postinstall
    ```

4. **Run the project**
    ```bash
    npm start
    ```

### Production mode

1. **Install**
    ```bash
    npm i -g @disruptivo/base-project-generator
    ```

2. **Run the command**
    ```bash
    generate-base-project
    ```

## How to publish a new version to NPM

1. **Run the following script in order to prepare the package, according your release**
    ```bash
    npm run release:YOUR_TYPE_OF_RELEASE // (e.g) npm run release:major
    ```

2. **Run the following command in order to publish the new version of the package**
    ```bash
    npm publish --access public
    ```

## Caveats

1. You can unpublish a package using `npm unpublish @YOUR_ORGANIZATION/YOUR_PACKAGE@YOUR_VERSION`
2. You can unpublish before 72 hours from the package has been published
3. You cannot publish a version that you unpublished previously